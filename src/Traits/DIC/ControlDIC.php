<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\DIC;

use Nette\Utils\ArrayHash;

trait ControlDIC {
    
    /**
     * Add input with DIC verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param ArrayHash $errorMessages Error messages for input: {'length' => string $lengthMessage}
     * @return InputDIC Input with zip filters and formatting
     */
    public function addDIC(string $name, string $label = null, ArrayHash $errorMessages = null): InputDIC
    {
        return $this[$name] = new InputDIC($label, $errorMessages);
    }
}
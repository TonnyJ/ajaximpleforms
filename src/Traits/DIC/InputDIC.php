<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\DIC;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\Strings;
use Nette\Utils\ArrayHash;
use Nette\Forms\Controls\BaseControl;

class InputDIC extends TextInput
{
    
    const 
        IS_DIC = '\AJAXimple\Forms\Traits\DIC\InputDIC::dicValidator',
        REGEXP = '(^([a-zA-Z]){2}\d{8,10}$)',
        ERROR_DIC_WRONG_FORMAT = 'error_dic';

    public function __construct(string $label = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setType('DIC');
        $this->setRequired(FALSE);
        
        $this->setAttribute('placeholder', 'AA00000000(00)');
        
        $this->addRule(
                self::IS_DIC, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::ERROR_DIC_WRONG_FORMAT) ? $errorMessages[self::ERROR_DIC_WRONG_FORMAT] : 'Wrong format of DIC.'));
        
        $this->addFilter([$this, 'dicFilter']);
    }

    public static function removeSpaces(string $value): string
    {
        return Strings::upper(preg_replace('#\s+#', '', $value));
    }
    /** 
     * Turn DIC to specified format for selected country
     * @param string $value Value from input to get correct format
     * @return string DIC in correct format
     */
    public function dicFilter(string $value): string
    {
        return self::removeSpaces($value);
    }

    /**
     * Check, if the value is valid dic code
     * @param Nette\Forms\Controls\BaseControl $item Control with value to check
     * @return bool True, if is valid, false otherwise
     */
    public static function dicValidator(BaseControl $item): bool
    {
        return Strings::match(self::removeSpaces($item->getValue()), self::REGEXP) !== NULL;
    }
}
<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\DateTime;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;

class InputDateTime extends TextInput
{
    
    const 
        DATE = '\AJAXimple\Forms\Traits\DateTime\InputDateTime::verifyDate',
        DATE_FORMAT = '\AJAXimple\Forms\Traits\DateTime\InputDateTime::verifyFormat',
        DATE_GREGORIAN = '\AJAXimple\Forms\Traits\DateTime\InputDateTime::verifyGregorian',
        DATE_WRONG = 'date',
        DATE_FORMAT_WRONG = 'format',
        DATE_GREGORIAN_WRONG = 'gregorian',
        IN_FORMAT = 'd.m.Y H:i';
        

    public function __construct(string $label = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setRequired(FALSE);
        
        
        $this->setType('datetime');
        $this->setOption('type', 'datetime');
        $this->addRule(
                self::DATE_FORMAT, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_FORMAT_WRONG) ? $errorMessages[self::DATE_FORMAT_WRONG] : 'Wrong format of date.'));
        $this->addRule(
                self::DATE, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_WRONG) ? $errorMessages[self::DATE_WRONG] : 'Wrong date.'));
        $this->addRule(
                self::DATE_GREGORIAN, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_GREGORIAN_WRONG) ? $errorMessages[self::DATE_GREGORIAN_WRONG] : 'You can use only dates after 15.10.1582.'));
        $this->addFilter([$this, 'outputFilter']);
        
    }
    
    /**
     * @param DateTime|string $input
     */
    public function outputFilter($input): ?DateTime
    {
        if($input instanceof DateTime){return $input;}
        if(empty($input)){return NULL;}
        return DateTime::createFromFormat(self::IN_FORMAT, $input);
    }

    /** 
     * Verify format of Date
     * @param TextInput $input
     * @return bool True if Date is in correct format, false otherwise
     */
    public static function verifyDate(TextInput $input): bool
    {
        if(($dateTime = $input->value) instanceof DateTime){return TRUE;}
        if(empty($dateTime)){return TRUE;}
        return !((bool) (date_parse_from_format(self::IN_FORMAT, $dateTime)['warning_count']));
    }
    
    /** 
     * Verify format of Date
     * @param TextInput $input
     * @return bool True if Date is in correct format, false otherwise
     */
    public static function verifyFormat(TextInput $input): bool
    {
        if(($date = $input->value) instanceof DateTime){return TRUE;}
        if(empty($date)){return TRUE;}
        return !((bool) (date_parse_from_format(self::IN_FORMAT, $date)['warning_count']));
    }
    
    /** 
     * Verify format of Date
     * @param TextInput $input
     * @return bool True if Date is in correct format, false otherwise
     */
    public static function verifyGregorian(TextInput $input): bool
    {
        if(!($date = $input->value) instanceof DateTime){
            if(empty($date)){return TRUE;}
            $date = DateTime::createFromFormat(self::IN_FORMAT, $date);
        }
        return $date > DateTime::fromParts(1582, 10, 14, 0, 0, 0);
    }
    
    /**
     * @param DateTime|\DateTime|string $date
     * 
     * @return void
     */
    public function setValue($date): void
    {
        if($date instanceof DateTime || $date instanceof \DateTime){$date = $date->format(self::IN_FORMAT);}
        parent::setValue($date);
    }
    
    /**
     * @param DateTime|\DateTime|string $date
     * 
     * @return void
     */
    public function setDefaultValue($date): void
    {
        if($date instanceof DateTime || $date instanceof \DateTime){$date = $date->format(self::IN_FORMAT);}
        parent::setDefaultValue($date);
    }
    
    /**
     * @return DateTime
     */
    public function getValue(): ?DateTime
    {
        $value = parent::getValue();
        if(empty($value)){return NULL;}
        return DateTime::createFromFormat(self::IN_FORMAT, $value) ?: null;
    }
}
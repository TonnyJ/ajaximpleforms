<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\DateTime;

use Nette\Utils\ArrayHash;

trait ControlDateTime
{

    /**
     * Add input with mobile phone verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param ArrayHash $errorMessages Error message for wrong format
     * @return InputDateTime Input with mobile filters and formatting
     */
    public function addDateTime(string $name, string $label = null, ArrayHash $errorMessages = null): InputDateTime
    {
        return $this[$name] = new InputDateTime($label, $errorMessages);
    }
}
<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\IC;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\ArrayHash;
use Nette\Forms\Controls\BaseControl;

class InputIC extends TextInput
{
    
    const 
        IS_IC = '\AJAXimple\Forms\Traits\IC\InputIC::icValidator',
        REGEXP = '#^\d{8}$#',
        ERROR_IC_WRONG_FORMAT = 'error_ic';

    public function __construct(string $label = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setType('IC');
        $this->setRequired(FALSE);
        
        $this->setAttribute('placeholder', 'XXXXXXXX');
        
        $this->addRule(
                self::IS_IC, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::ERROR_IC_WRONG_FORMAT) ? $errorMessages[self::ERROR_IC_WRONG_FORMAT] : 'Wrong format of IC.'));
        
        $this->addFilter([$this, 'icFilter']);
    }

    public static function removeSpaces(string $value): string
    {
        return preg_replace('#\s+#', '', (string) $value);
    }
    
    public function icFilter(string $value): string 
    {
        return self::removeSpaces($value);
    }
    
    /** 
     * Verify format of IC
     * @param int $ic IC
     * @return bool True if IC is in correct format
     * 
     * @author David Grudl [2007]
     */
    public static function verifyIC(string $ic): bool
    {
        $ic = self::removeSpaces($ic);

        if (!preg_match(self::REGEXP, $ic)) {
            return false;
        }
        
        $a = 0;
        for ($i = 0; $i < 7; $i++) {
            $a += $ic[$i] * (8 - $i);
        }

        $a = $a % 11;
        if ($a === 0) {
            $c = 1;
        } elseif ($a === 1) {
            $c = 0;
        } else {
            $c = 11 - $a;
        }

        return $ic[7] === $c;
    }
    
    /**
     * Check, if the value is valid ic code
     * @param Nette\Forms\Controls\BaseControl $item Control with value to check
     * @return bool True, if is valid, false otherwise
     */
    public static function icValidator(BaseControl $item): bool
    {
        return self::verifyIC($item->getValue());
    }
}
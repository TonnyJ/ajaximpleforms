<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\IC;

use Nette\Utils\ArrayHash;

trait ControlIC
{
    
    /**
     * Add input with IC verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param ArrayHash $errorMessages Error messages for input: {'length' => string $lengthMessage}
     * @return IC Input with zip filters and formatting
     */
    public function addIC(string $name, string $label = null, ArrayHash $errorMessages = null): InputIC
    {
        return $this[$name] = new InputIC($label, $errorMessages);
    }
}
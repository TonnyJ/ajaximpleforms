<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\CheckBoxListBits;

use Nette\Forms\Controls\CheckboxList;

class InputCheckBoxListBits extends CheckboxList
{
    
    const 
        DATE = '\AJAXimple\Forms\Traits\Date\InputDate::verifyDate',
        DATE_FORMAT = '\AJAXimple\Forms\Traits\Date\InputDate::verifyFormat',
        DATE_GREGORIAN = '\AJAXimple\Forms\Traits\Date\InputDate::verifyGregorian',
        DATE_WRONG = 'date',
        DATE_FORMAT_WRONG = 'format',
        DATE_GREGORIAN_WRONG = 'gregorian',
        IN_FORMAT = 'd.m.Y';
        

    public function __construct(string $label = null, array $items = null)
    {
        parent::__construct($label, $items);
        $this->setRequired(FALSE);
    }
    
    public function getValue(): int
    {
        $prom = parent::getValue();
        $retVal = 0;
        foreach ($prom as $value) {
            $retVal += (1 << $value);
        }
        return $retVal;
    }
    
    /**
     * @param int|array $value If it is array, so set this array. If it is int, then filter this int to array.
     * 
     * @return void
     */
    public function setValue($value): void
    {
        if (!is_array($value)) {
            $mask = 1;
            $tmp = [];
            $i = 0;
            while ($value > 0) {
                if ($value & $mask) {
                    $tmp[] = $i;
                }
                $value = $value >> 1;
                $i++;
            }
            $value = $tmp;
        }
        parent::setValue($value);
    }
}
<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\CheckBoxListBits;

trait ControlCheckBoxListBits 
{

    /**
     * Add input with checkBoxList which has nice formatted input and output
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param ArrayHash $items Items for checkbox
     * 
     * @return InputCheckBoxListBits Input with checkBoxList which input and output is formatted by bit mask (so it's int)
     */
    public function addCheckBoxListBits(string $name, string $label = null, array $items = null): InputCheckBoxListBits
    {
        return $this[$name] = new InputCheckBoxListBits($label, $items);
    }
}
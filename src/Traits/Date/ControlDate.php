<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\Date;

use Nette\Utils\ArrayHash;

trait ControlDate
{

    /**
     * Add input with mobile phone verify and formatting
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @param ArrayHash $errorMessages Error message for wrong format
     * @return InputDate Input with mobile filters and formatting
     */
    public function addDate(string $name, string $label = null, ArrayHash $errorMessages = null): InputDate
    {
        return $this[$name] = new InputDate($label, $errorMessages);
    }
}
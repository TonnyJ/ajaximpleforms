<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\Date;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;

class InputDate extends TextInput
{
    const 
        DATE = '\AJAXimple\Forms\Traits\Date\InputDate::verifyDate',
        DATE_FORMAT = '\AJAXimple\Forms\Traits\Date\InputDate::verifyFormat',
        DATE_GREGORIAN = '\AJAXimple\Forms\Traits\Date\InputDate::verifyGregorian',
        DATE_RANGE = '\AJAXimple\Forms\Traits\Date\InputDate::isDateInRange',
        DATE_WRONG = 'date',
        DATE_FORMAT_WRONG = 'format',
        DATE_GREGORIAN_WRONG = 'gregorian',
        IN_FORMAT = 'd.m.Y';
        

    public function __construct(string $label = null, ArrayHash $errorMessages = null)
    {
        parent::__construct($label);
        $this->setRequired(FALSE);
        
        
        $this->setType('date');
        $this->setOption('type', 'date');
        $this->addRule(
                self::DATE_FORMAT, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_FORMAT_WRONG) ? $errorMessages[self::DATE_FORMAT_WRONG] : 'Wrong format of date.'));
        $this->addRule(
                self::DATE, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_WRONG) ? $errorMessages[self::DATE_WRONG] : 'Wrong date.'));
        $this->addRule(
                self::DATE_GREGORIAN, 
                ($errorMessages !== NULL && $errorMessages->offsetExists(self::DATE_GREGORIAN_WRONG) ? $errorMessages[self::DATE_GREGORIAN_WRONG] : 'You can use only dates after 15.10.1582.'));
        $this->addFilter([$this, 'outputFilter']);
        
    }
    
    public function outputFilter($input): ?DateTime
    {
        if($input instanceof DateTime){return $input;}
        if(empty($input)){return NULL;}
        list($day, $month, $year) = explode('.', $input);
        return DateTime::fromParts((int)$year, (int)$month, (int)$day, 0, 0, 0);
    }

    /** 
     * Verify format of Date
     * @param TextInput $input
     * @return bool True if Date is in correct format, false otherwise
     */
    public static function verifyDate(TextInput $input): bool
    {
        if(($date = $input->value) instanceof DateTime){return TRUE;}
        if(empty($date)) {return TRUE;}
        list($day, $month, $year) = explode('.', $date);
        return checkdate((int)$month, (int)$day, (int)$year);
    }
    
    /** 
     * Verify format of Date
     * @param TextInput $input
     * @return bool True if Date is in correct format, false otherwise
     */
    public static function verifyFormat(TextInput $input): bool
    {
        if(($date = $input->value) instanceof DateTime){return TRUE;}
        if(empty($date)) {return TRUE;}
        return !((bool) (date_parse_from_format('d.m.Y', $date)['warning_count']));
    }
    
    /** 
     * Verify format of Date
     * @param TextInput $input
     * @return bool True if Date is in correct format, false otherwise
     */
    public static function verifyGregorian(TextInput $input): bool
    {
        if(!($date = $input->value) instanceof DateTime){
            if(empty($date)) {return TRUE;}
            list($day, $month, $year) = explode('.', $date);
            $date = DateTime::fromParts((int)$year, (int)$month, (int)$day, 0, 0, 0);
        }
        return $date > DateTime::fromParts(1582, 10, 14, 0, 0, 0);
    }
    
    /** 
     * Verify format of Date
     * @param TextInput $input
     * @param DateTime[] $rules
     * @return bool True if Date is in given range, false otherwise
     */
    public static function isDateInRange(TextInput $input, array $rules): bool
    {
        if (!($date = $input->getValue()) || !$date instanceof DateTime) {
            return false;
        }
        
        [$min, $max] = $rules;
        
        if (!$min instanceof DateTime || !$max instanceof DateTime) {
            return false;
        }
        
        return $date > $min && $date < $max;
    }
    
    /**
     * @param DateTime|\DateTime|string $date
     * 
     * @return void
     */
    public function setValue($date): void
    {
        if($date instanceof DateTime || $date instanceof \DateTime){$date = $date->format(self::IN_FORMAT);}
        parent::setValue($date);
    }
    
    /**
     * @param DateTime|\DateTime|string $date
     * 
     * @return void
     */
    public function setDefaultValue($date): void
    {
        if($date instanceof DateTime || $date instanceof \DateTime){$date = $date->format(self::IN_FORMAT);}
        parent::setDefaultValue($date);
    }
    
    /**
     * @return DateTime
     */
    public function getValue(): ?DateTime
    {
        $value = parent::getValue();
        if (empty($value)) {return NULL;}
        return DateTime::createFromFormat(self::IN_FORMAT, $value) ?: null;
    }
}
<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\Container;

use Nette\Forms\Container;
use AJAXimple\Forms\Traits\ControlsAll;

class InputContainer extends Container
{
    use ControlsAll;
}
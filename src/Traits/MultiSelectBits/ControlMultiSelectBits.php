<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\MultiSelectBits;

trait ControlMultiSelectBits
{

    /**
     * Add input with MultiSelect which has nice formatted input and output
     * @param string $name Name of the input
     * @param string $label Label for the input
     * @return InputMultiSelectBits Input with MultiSelect which input and output is formatted by bit mask (so it's int)
     */
    public function addMultiSelectBits(string $name, string $label = null, array $items = null): InputMultiSelectBits
    {
        return $this[$name] = new InputMultiSelectBits($label, $items);
    }
}
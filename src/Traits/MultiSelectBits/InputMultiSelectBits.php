<?php

declare(strict_types=1);

namespace AJAXimple\Forms\Traits\MultiSelectBits;

use Nette\Forms\Controls\MultiSelectBox;
use Nette\Utils\ArrayHash;

class InputMultiSelectBits extends MultiSelectBox
{
    
    public function __construct(string $label = null, string $items = null)
    {
        parent::__construct($label, $items);
        $this->setRequired(FALSE);
    }
    
    public function getValue(): int
    {
        $prom = parent::getValue();
        $retVal = 0;
        foreach ($prom as $value) {
            $retVal += (1 << $value);
        }
        return $retVal;
    }
    
    /**
     * @param int|array $value
     */
    public function setValue($value): void
    {
        if (!is_array($value)) {
            $mask = 1;
            $tmp = [];
            $i = 0;
            while ($value > 0) {
                if ($value & $mask) {
                    $tmp[] = $i;
                }
                $value = $value >> 1;
                $i++;
            }
            $value = $tmp;
        }
        parent::setValue($value);
    }
}
<?php

declare(strict_types=1);

namespace AJAXimple\Forms;

use Nette\Application\UI;
use AJAXimple\Forms\Traits\Date\InputDate;

class Form extends UI\Form
{
    use Traits\ControlsAll;
    
    const 
        DATE_RANGE = InputDate::DATE_RANGE,
        DATE_TIME_RANGE = self::DATE_RANGE;
}

<?php

declare(strict_types=1);

namespace AJAXimple\Forms;

use Nette\DI\CompilerExtension;

class AJAXimpleFormsExtension extends CompilerExtension
{
    public function loadConfiguration(): void
    {
        // načtení konfiguračního souboru pro rozšíření
        $this->compiler->loadDefinitionsFromConfig(
                $this->loadFromFile(__DIR__ . '/config/common.neon')['services'],
        );
    }
}

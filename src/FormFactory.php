<?php

declare(strict_types=1);

namespace AJAXimple\Forms;

use AJAXimple\Forms\Form;

interface FormFactory
{
    /**
     * @return Form Form with added traits
     */
    public function create(): Form;
}
